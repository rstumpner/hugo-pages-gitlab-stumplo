[![pipeline status](https://gitlab.com/rstumpner/stumplo-hugo-pages/badges/master/pipeline.svg)](https://gitlab.com/rstumpner/stumplo-hugo-pages/-/commits/master)


---

Example [Hugo] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---
## stumplo-hugo-pages

This is the Repository of a example or hello world app. The main function ist to convert markdown files into a Website and Publish this content.The goal of this very simple web application is to show the architecture and mechanics of different technologies and deployment technics with their workflow.

In this Repository the technology stack is:
- Sources: markdown
- application: Static Site Genereator [hugo.io](https://hugo.io)
- environment: Gitlab Pages 

## Workflow
- Create a Gitlab Issue (plan)
- Clone the Gitlab Repository `git clone  https://gitlab.com/rstumpner/stumplo-hugo-pages.git` (code)
- Create a Gitlab Merge Request
- change local Git Repository to the new branch 
- Create Changes in the Content directory `content/post/`
- commit changes to the new branch
- push changes to gitlab
- now the included CI/CD Pipeline from gitlab `.gitlab-ci.yml` generate a static website from the content
- merge the branch to main
- now the included CI/CD Pipeline from gitlab `.gitlab-ci.yml` publish the static website to the Gitlab Pages Service
 
## Building it locally

To work locally with this project, you'll have to follow the steps below:

- Fork, clone or download this project
- [Install][] Hugo
- Preview your project: `hugo server`
- Add content
- Generate the website: `hugo` (optional)


### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
